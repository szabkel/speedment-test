package com.speedmenttest.speedmenttest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpeedmenttestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpeedmenttestApplication.class, args);
	}
}
